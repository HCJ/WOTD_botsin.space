import requests
from bs4 import BeautifulSoup

def scrape_word_and_definition():
    url = 'https://www.merriam-webster.com/word-of-the-day'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    word_element = soup.find('h2', class_='word-header-txt')
    definition_element = soup.find('div', class_='wod-definition-container')
    
    if word_element and definition_element:
        word = word_element.text.strip()
        definition = definition_element.get_text(separator=' ').strip()
        return word, definition
    else:
        return None, None

if __name__ == "__main__":
    word, definition = scrape_word_and_definition()
    if word and definition:
        print(f"Word: {word}\nDefinition: {definition}")
